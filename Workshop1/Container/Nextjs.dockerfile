FROM node:18.17
#RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json yarn.lock ./
#RUN yarn install --frozen-lockfile
RUN yarn

COPY . .

RUN yarn build

#ENV NODE_ENV production

#RUN addgroup --system --gid 1001 bloggroup
#RUN adduser --system --uid 1001 bloguser

RUN chmod -R 777 /app
#USER bloguser

EXPOSE 3000

#ENV PORT 3000

CMD ["yarn", "start"]